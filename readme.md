The idea is to allow sites hosted in local networks such as home lans and intranets to be accessible from the Internet in a simple way. 

Our goal is to be able to use domestic equipment (HD, servers, etc) on the public internet without the need for VPN platforms such as zerotier, ngrok, wireguard and without doing port forwards on your home router.

It requires, in any case, a VPS with public ip accessible through ssh. It can be the cheapest available, since the storage and CPU used are from home.

There are a couple of methods proposed, depending on how you setup your VPS.

## Total tunnel
The first method tunnels all https/http connections on the VPS to your local machine. That means you can't run any web service on the VPS, but it is the simplest method.

### Requirements:
* In the local server (for instance, a raspberry pi)
	* Docker
	* Access to the VPS through ssh with key pair (public/private) as **root**
(ie you should be able to ssh root@x.xx.xxx.x)
* If you want to use https / SSL (highly recommended) you must have a domain pointed to the ip address of the VPS

### Instructions:

Local server
1. Login to your local server terminal
2. Make sure you can ssh from your local server to the VPS, by creating a private/public key pair and uploading the public key to the VPS. If you don't know how to do it, follow instructions from you VPS provider or a tutorial such as https://www.howtogeek.com/168147/add-public-ssh-key-to-remote-server-in-a-single-command/
3. On the remote VPS, edit the file /etc/ssh/sshd_config, and uncomment or add a line that says
	```
	GatewayPorts yes
	```
4. Restart the sshd service 
   ```
   service sshd restart
	```

5. Back to the local machine, clone this repository using git. If you don't have git installed, use sudo apt install git to install it
	```
	git clone https://gitlab.com/coolab-coletivo/local-tunel-ssh
	```
6. Change to the *total* folder. Edit the .env file to reflect the user and host of your VPS. You can keep the identity file or change them to your preference.
7. Bear in mind that the docker-compose only has the most simple nginx configuration, without certificate generation. You can edit it to use it with https://nginxproxymanager.com/ or https://github.com/nginx-proxy/nginx-proxy, which take care of certificates for you, or generate your own configuration. There is an example configuration of nginx-proxy included.
   It is important that the name of the service running nginx is 'webservice', since this is where the ssh tunnels will send the http/https requests to.
8. Start docker with 
   ```
   docker-compose up -d
   ```



## VPS with nginx proxy
This is an option when you cannot ssh as root to your VPS, or if you have some sites served by the VPS and others served at home.


### Requirements:
* In the local server (for instance, a raspberry pi)
	* Docker
	* NGINX installed directly or through Docker
	* Access to the VPS through ssh with key pair (public/private)
* In the public access VPS
	* NGINX installed

The local machine will open an ssh tunnel to the public VPS. Once https requests arrive at the VPS, nginx will reroute then to the local machine through the tunner.

### Instructions:


VPS:
1. Login to your VPS with ssh.
2. If you will use a nginx installation, add this site configuration file. On ubuntu/debian, it will be in /etc/nginx/sites-available/ . Take the code snippet below and save it there as mydomain.tld.conf (replacing mydomain.tld by the domain you want to forward to the local machine)
```
server {
	root /var/www/html;
	index index.html index.htm index.nginx-debian.html;
    server_namemydomain.tld; # change this to your domain


	location / {
            proxy_pass http://127.0.0.1:9000;
            proxy_next_upstream error  timeout invalid_header http_500 http_502 http_503;
            proxy_set_header X-Forward-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_ssl_protocols TLSv1 TLSv1.1 TLSv1.2 TLSv1.3;
            proxy_ssl_server_name on;
            proxy_set_header Access-Control-Allow-Credentials true;
            proxy_read_timeout 5m;
            proxy_set_header Host $host;
	}

}
```

3. Enable the site by making a link to it in the folder sites-enabled and restart the nginx service:
   
```
sudo ln -s /etc/nginx/sites-available/mydomain.tld.conf /etc/nginx/sites-enabled/mydomain.tld.conf

sudo service nginx restart

```
   
4. Now you need to create a SSL certificate for the site. The easiest way is to install certbot and let it handle it.
	```
	sudo apt update && sudo apt install certbot
	sudo certbot
	```

	Answer the questions and let certbot update your nginx configuration
   



Local server:
1. Login to your local server terminal
2. Make sure you can ssh from your local server to the VPS, by creating a private/public key pair and uploading the public key to the VPS. If you don't know how to do it, follow instructions from you VPS provider or a tutorial such as https://www.howtogeek.com/168147/add-public-ssh-key-to-remote-server-in-a-single-command/
3. Clone this repository using git. If you don't have git installed, use sudo apt install git to install it
4. Now this step depends on what service you're using in the local server.
	* The docker-compose.yml has the settings for a plain nginx installation listening on port 80 (the SSL is handled by the VPS nginx, so no worries here)
	* Other services will have specific settings. We added a hedgedoc.yml file to demonstrate the local installation of hedgedoc. A few notes:
		* It uses port 3000 instead of 80, so the command line that starts ssh service also uses this port
		* In the same line, the host is named *hedgedoc*, like the name of the hedgedoc service (in the docker-compose.yml it is named nginx)
		* The following settings in hedgedoc are important:

			 -- CMD_DOMAIN=mydomain.tld

			 -- CMD_URL_ADDPORT=false #optional

             -- CMD_PROTOCOL_USESSL=true #optional
	* If the service is in a separate docker-compose file, you will need to create and use a docker network in all services. You need to add it as an external network in all docker-compose files.
		* https://docs.docker.com/engine/reference/commandline/network_create/
		* https://docs.docker.com/compose/compose-file/#external
		  
5. Go to the local folder. Edit the .env file to reflect the user and host of your VPS. You can keep the local port and the identity file or change them to your preference.
6. Start docker with docker-compose up -d


